import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { ClienteComponent } from './cliente/cliente.component';
import { BarraComponent } from './barra/barra.component';
import { AddClienteComponent } from './add-cliente/add-cliente.component';
import { PesquisaComponent } from './pesquisa/pesquisa.component';


@NgModule({
  declarations: [
    AppComponent,
    ClienteComponent,
    BarraComponent,
    AddClienteComponent,
    PesquisaComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
