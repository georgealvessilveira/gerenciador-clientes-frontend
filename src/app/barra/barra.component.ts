import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-barra',
  templateUrl: './barra.component.html',
  styleUrls: [
    './barra.component.css',
    '../app.component.css'
  ]
})
export class BarraComponent implements OnInit {

  @Input() titulo: string;

  constructor() { }

  ngOnInit() {
    this.titulo = "Gerenciador de Clientes";
  }

}
